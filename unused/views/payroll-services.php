<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/Payroll.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Payroll Services</h1>
            <span>Real-time Access to Payroll Processing, Reporting, and Tracking Information Your Employees Rely On Most</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Payroll-Employee-Access.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Payroll Made Easy</h4>
                    <div class="inside-spacer"></div>
                    <p>Payroll is not one of the business areas that you can afford to cut corners. Seasoned payroll technicians at VensureHR ensure that a typically
                        complex process involving current knowledge of tax laws is made simple by eliminating the hidden costs of compliance and risks created by ever-changing regulations.</p>
                    <p><strong>Payroll</strong> processing and administration services are offered by VensureHR as a comprehensive answer to your various business needs. Finally,
                        real-time access to the information your employees rely on most.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Payroll Administrative Services</h4>
                    <ul class="list-unstyled m-l-20">
                        <li class="p-t-10 p-b-5">Prepare, file, and settle payroll taxes</li>
                        <li class="p-t-10 p-b-5">Issue payroll checks and/or direct deposits</li>
                        <li class="p-t-10 p-b-5">Unlimited reporting</li>
                        <li class="p-t-10 p-b-5">Issue annual W-2s</li>
                        <li class="p-t-10 p-b-5">Garnishments and unemployment claims management</li>
                        <li class="p-t-10 p-b-5">Track employee paid personal and sick leave</li>
                    </ul>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Payroll-Check.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-50"></div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Payroll-Secure-Access.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Secure Online Employer Access</h4>
                    <ul class="list-unstyled m-l-20">
                        <li class="p-t-10 p-b-5">Secure, real-time, mobile access to payroll and human resource data.</li>
                        <li class="p-t-10 p-b-5">User personalization settings</li>
                        <li class="p-t-10 p-b-5">Universal manual entry and/or import capabilities</li>
                        <li class="p-t-10 p-b-5">Remote payroll approval</li>
                        <li class="p-t-10 p-b-5">Manual batch creation for checks needed between regular payroll processing</li>
                        <li class="p-t-10 p-b-5">Access to payroll, human resources, census reports, and invoices</li>
                        <li class="p-t-10 p-b-5">	Current and historical pay stubs and W-2s</li>
                    </ul>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h4>Employee Self-Service Portal</h4>
                <ul class="list-unstyled m-l-20">
                    <li class="p-t-10 p-b-5">Anytime, anywhere access to employee profiles</li>
                    <li class="p-t-10 p-b-5">Printable check stubs and W-2s</li>
                    <li class="p-t-10 p-b-5">Complete direct deposit information</li>
                    <li class="p-t-10 p-b-5">Current tax withholdings, elections, and year-to-date totals</li>
                </ul>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Payroll-Mobile.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">HR Solutions That Deliver You the Freedom to Succeeds</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-20"></div>
</section>