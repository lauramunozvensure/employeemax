<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/Size-based.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Size Based Solutions</h1>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Size-Based-Solutions-1.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <h4>Solutions for Every Size</h4>
                <div class="inside-spacer"></div>
                <p>VensureHR offers a multitude of solutions designed to meet the various needs of your growing business. From a small business owner to the CEO of a
                    Fortune 500 company, we connect tailored PEO services and solutions with your organization, regardless of size.</p>
                <p><a href="<?php echo basePathUrl();?>peo-services/vfficient" class="internal">Vfficient™ VensureHR’s Human Capital Management Technology</a> (HCM) is your answer to simplify your payroll, benefits, and human resources (HR).
                    By integrating these core services into an intuitive, cloud based single sign-on platform, you save time and reduce costs, further helping your
                    business grow to its full potential.</p>
            </div>
        </div>
        <div class="section-spacer-40"></div>
        <div id="portfolio" class="grid-layout portfolio-3-columns" data-margin="20">
            <div class="portfolio-item no-overlay img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="<?php echo basePathUrl();?>solutions/small-business"><img src="<?php echo basePathUrl();?>images/Business-Solutions-Small1.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="portfolio-item no-overlay img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="<?php echo basePathUrl();?>solutions/midsize-business"><img src="<?php echo basePathUrl();?>images/Business-Solutions-Mid1.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="portfolio-item no-overlay img-zoom">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="<?php echo basePathUrl();?>solutions/large-business"><img src="<?php echo basePathUrl();?>images/Business-Solutions-Big2.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="shape-2-outside-top shape-top">
    <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-grey-divider-top-right.png" />
</div>
<section class="background-grey p-t-50">
    <div class="section-spacer-30"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 p-r-40">
                <h4>Change How You Manage</h4>
                <div class="inside-spacer"></div>
                <p>With thousands of clients across the US, we have changed the way you manage people, retain talent, accomplish goals, and improve your company culture.
                    Regain organizational peace-of-mind with the technology and industry best practices to accelerate your business —VensureHR will take it from here.</p>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Vensure-HR-Size-Based-Img1.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-50"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Are You Ready to Grow Your Business With VensureHR?</h4>
                <p class="m-t-60 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-30"></div>
</section>
