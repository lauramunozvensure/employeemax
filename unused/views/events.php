<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Events.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Events</h1>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-right.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-30"></div>
        <div class="row text-center">
            <div class="col-lg-12">
                <h3>Please check back for 2020 events.</h3>
            </div>
        </div>
        <div class="section-spacer-30"></div>
    </div>
</section>

