<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/VensureHR-Mid-Market-Solutions.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Mid-Market Solutions</h1>
            <span>Partnering for Growth</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <a href="#"><img src="<?php echo basePathUrl();?>images/Vensure-HR-Midsize-Business-Img.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>50-500 Employees</h4>
                    <div class="inside-spacer"></div>
                    <p>It is easy to feel overwhelmed with all the uncertainty surrounding rapidly changing and new regulations, employer liability,
                        and trying to offer an affordable and competitive benefits solution for your employees.</p>
                    <p>VensureHR’s solutions enhances the existing framework of your business with expert staff who can assist in handling employee
                        garnishments, unemployment claims, and payroll tax needs, including submitting tax payments, filing returns, and issuing annual W2s.</p>
                    <p>Seamlessly combining modern HR technology through our Vfficient HCM and expertly trained HR professionals. We provide the most
                        comprehensive solution designed specifically for your business.</p>
                </div>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Are You Ready to Grow Your Business With VensureHR?</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
        <div class="section-spacer-40"></div>
    </div>
</section>

