<div id="slider" class="vensure-slider slider-fullscreen dots-creative" data-height-xs="360">
    <div class="slide kenburns" data-bg-parallax="<?php echo basePathUrl();?>images/home/vensure-landingpage-bg.jpg">
        <div class="bg-overlay" data-style="referrals"></div>
        <div class="container">
            <div class="slide-captions">
                <div class="row">
                    <div class="col-lg-8" data-animate="fadeInUp" data-animate-delay="500">
                        <h1 data-animate="fadeInUp" data-animate-delay="500" class="referrals">Full-Service BPO solutions by Solvo Global tailored just for you.</h1>
                        <h2 data-animate="fadeInUp" data-animate-delay="1000" class="referrals-subtitle">A nearshore service provider for US companies who are looking to outsource business functions or transfer employees to a less expensive region near their headquarters. Bolster your organization with highly qualified bilingual personnel all while improving profitability.</h2>
                        <h3 data-animate="fadeInUp" data-animate-delay="1500" class="referrals-text"></h3>
                    </div>
                    <div class="col-lg-4" data-animate="fadeInUp" data-animate-delay="1500">
                        <div class="card referrals">
                            <div class="card-body">
                                <img src="<?php echo basePathUrl();?>images/solvo-logo.png" class="img-fluid mx-auto d-block" />
                                <form class="form-referrals" novalidate action="<?php echo basePathUrl();?>form-send/referrals" role="form" method="post">
                                    <iframe src="https://go.vensure.com/l/656143/2020-03-11/vws13" width="100" height="425" scrolling="no" type="text/html" frameborder="0" allowTransparency="true" style="border: 0" scrolling="no"></iframe>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section id="page-content">
    <div class="container">
        <div class="grid-system-demo-live">
            <div class="row">
                <div class="col-lg-12 p-t-40 p-b-20">
                    <div class="heading-text heading-section referrals">
                        <h2 class="text-center referrals-text">About Solvo Global</h2>
                        <div class="line-small"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <a href="#"><img src="<?php echo basePathUrl();?>images/marketplace/Solvo-Intro-Thumbnail.jpg" alt=""></a>
                        </div>
                        <div class="portfolio-description">
                            <a data-lightbox="iframe" href="https://vimeo.com/301057548?autoplay=1">
                                <i class="fas fa-play"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 p-l-40">
                    <p>Solvo Global is a business process outsourcing (BPO) company based in Medellin, Colombia.  We do things a little differently than a traditional BPO.  First of all, we take care of our people.  Our representatives work in state-of-the-art facilities and not backroom sweatshops. We call our workers in Colombia Soulvers because they put their hearts and souls into what they do.   Secondly, we provide the best transparency and control out of any BPO in the industry.  We pride ourselves in our ability to customize our process to each of our client's needs.  We can do everything from providing Real-Time cameras to ensure your peace of mind, to creating customized dashboards to provide as much data as possible.   Finally, our Soulvers are 100 percent dedicated to you.  There is no talent sharing; you get a dedicated employee while we take care of all of the risks at a fraction of the price.</p>
                    <p>With Solvo Global you can confidently fill a variety of positions in your company for a fraction of the cost of hiring and paying an employee.  Vensure Employer Services has partnered with Solvo Global to offer our clients discounted average rate of $9 an hour.  Our clients have reported saving over $300,000 a year for every ten positions filled by Solvo Global employees.</p>
                    <p>Additionally, work with Solvo comes with no long-term contracts, legal responsibilities, liabilities, exposures, or high initial investments and saves you costs on benefits.</p>
                    <p>With a wide range of expertise and talent, Solvo Global employees can help your company meet their specific needs.  Solvo Global understands that growing companies have a variety of requirements that its employees are able to meet.  Solvo Global provides workforce support in administration, customer service, inside sales, documentation, dispatching, and bookkeeping, to name a few.  Some of the more recent job positions Solvo employees have filled range from executive assistant to payroll administrator and from sales development reps to web developer. </p>
                    <p>Solvo Global is committed to lowering your operational and labor costs and is a wonderful solution to growing your business.</p>
            </div>
        </div>
    </div>
</section>
<section id="page-content">
    <div class="container">
        <div class="grid-system-demo-live">
            <div class="row">
                <div class="col-lg-12 p-t-40 p-b-20">
                    <div class="heading-text heading-section referrals">
                        <h2 class="text-center referrals-text">Get Started Today</h2>
                        <div class="line-small"></div>
                    </div>
                    <img src="<?php echo basePathUrl();?>images/solvo-logo.png" class="img-fluid mx-auto d-block" />
                    <iframe src="https://go.vensure.com/l/656143/2020-03-11/vws13" width="100" height="425" scrolling="no" type="text/html" frameborder="0" allowTransparency="true" style="border: 0" scrolling="no"></iframe>
            </div>
        </div>
    </div>
</section>
