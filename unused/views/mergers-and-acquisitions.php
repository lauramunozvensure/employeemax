<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/vensure-hr-mergers-aquisitions.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Mergers and Acquisitions</h1>
            <span>Become Part of a Nationally Recognized Team</span>
        </div>
    </div>
    <div class="shape-1-inside-bottom shape-bottom">
        <img src="<?php echo basePathUrl();?>images/overlay/Vensure-HR-divider-top-left.png" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <a href="#"><img src="<?php echo basePathUrl();?>images/vensure-hr-mergers-aquisitions-growth.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Supporting Your Organization’s Growth Strategy</h4>
                    <div class="inside-spacer"></div>
                    <p>Vensure Employer Services works closely with PEO and HR companies across the country to help develop and support their growth strategies.
                        For some business owners the decision to continue their organization’s growth means turning to Vensure.</p>
                    <p>Becoming a Division Partner with Vensure Employer Services means:</p>
                    <ul class="list-icon dots list-icon-list list-icon-colored-grey">
                        <li>Employee Security</li>
                        <li>Continued investment in your industry</li>
                        <li>Industry-leading technology</li>
                        <li>Clients gain access to benefits typically reserved for Fortune 500-level employers</li>
                    </ul>
                    <p>If you’re ready to become part of a nationally recognized team while maintaining your brand and growing your client-base, reach out to Vensure today!</p>
                </div>
            </div>
        </div>
        <div class="section-spacer-60"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Grow Your Business With VensureHR Today!</h4>
                <p class="m-t-30 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
            </div>
        </div>
        <div class="section-spacer-40"></div>
    </div>
</section>

