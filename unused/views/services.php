<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/home/Atlas-Services-Hero.jpg">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="page-title">
            <h1 class="">Services Offered at Atlas Professional Services</h1>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Atlas-Services-Payroll.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Payroll</h4>
                    <div class="inside-spacer"></div>
                    <p>Atlas assists clients in all aspects of payroll preparation and reporting.</p>
                    <p>We manage our clients’ payroll records and conduct regular payroll and IRS audits to ensure accuracy of records. Atlas assists clients with calculating, deducting, and remitting all federal, state, and local taxes, as well as unemployment taxes. Our certified payroll professionals can assist with federal, state, and local forms, including W-2s, garnishment deductions, IRS tax levies, and Friend of the Court payments.</p>
                    <p>Utilizing our web-based payroll reporting, clients can take advantage of standard and customized payroll reports, as well as quarterly reports for FICA, FUTA, and SUTA.</p>
                    <p>Atlas’s certified payroll processing services include processing bonuses and commissions, direct deposits, and ACH billing. We also manage vacation, personal time, and sick pay accruals.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-10"></div>
</section>

<section class="background-grey p-t-50">
    <div class="section-spacer-40"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Benefit Services</h4>
                    <div class="inside-spacer"></div>
                    <p>Atlas frees business owners from administrative burdens and allows businesses to compete more aggressively. By offering clients top-notch benefits that may not be available to the competition, Atlas allows companies to attract and retain employees.</p>
                    <p>Atlas has the unique advantage of offering our clients larger benefit buying power in the ever-expanding marketplace of health insurance, as well as additional “value-added” benefits such as dental, vision, life, disability, 401(k), and flex plans. We tailor a benefits program specific to clients’ worksites and employee needs, while alleviating the headaches of the open enrollment process, COBRA compliance, and HIPAA regulations.</p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Atlas-Services-Benefits.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="container">
            <h3>Benefit Programs Include:</h3>
          <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="list-icon dots list-icon-list list-icon-colored-grey">
                            <li>Benefit administration, including quotes on benefit plans, process billing, automatic payroll deduction for all selected benefits, and assistance in claim inquiries.</li>
                            <li>Group benefit plans, including dental, vision, and life insurance that can be either employer sponsored or a voluntary election for employees.</li>
                            <li>Delivering all services involved with maintaining COBRA and HIPAA compliance and their requirements, including providing guidance and doing any associated reporting.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="list-icon dots list-icon-list list-icon-colored-grey">
                            <li>Flexible spending for employees who want to set aside pre-tax funds for medical and dependent care expenses.</li>
                            <li>A platform of top-rated 401(k) funds for employees to choose from, where money will be invested directly from payroll deduction.</li>
                            <li>AFLAC, which includes many policies that cover accident, intensive care, cancer, hospital, and recovery.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p class="m-t-60 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a>
                </p>
            </div>
        </div>
    </div>
    <div class="section-spacer-10"></div>
</section>

<section>
    <div class="container">
        <div class="section-spacer-20"></div>
        <div class="row">
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Atlas-Services-HR.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 p-l-40">
                <div>
                    <h4>Human Resource Services</h4>
                    <div class="inside-spacer"></div>
                    <p>Atlas’s highly skilled human resources staff is available to assist with all clients’ HR needs. We help organizations avoid costly mistakes while navigating through regulatory complexities. Our HR services include consultation for termination procedures and immediate troubleshooting support, as well as administration for records, drug screening, motor vehicle reports, and background checks. Atlas ensures clients meet compliance standards with ADA, FMLA, EEO, INS/I-9, as well as wage and hour law (FLSA) and employment regulations. Additionally, Atlas offers employee handbooks and unemployment claims management and we keep clients abreast of HR legal trends, legislation updates, and all other relevant topics.</p>
                    <p class="m-t-30"><a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="section-spacer-10"></div>
</section>

<section class="background-grey p-t-50">
    <div class="section-spacer-40"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div>
                    <h4>Safety and Risk Services</h4>
                    <div class="inside-spacer"></div>
                    <p>Atlas provides a wide variety of services geared towards increasing workplace safety, health, compliance, and profitability. Our staff will work closely with clients to help realize their businesses’ full potential.</p>
                    <p>We identify the safety and health liabilities associated with our clients’ businesses. Atlas’s expertise in the safety and health fields along with our dedication to our clients enables us to provide affordable and timely occupational health and safety prevention programs.</p>
                    <p>Atlas’s goal is to respond intelligently and proactively to client needs with the professional and highly technical skills required for innovative solutions to today’s complex occupational safety and health issues. We realize that every business is unique, obligating us to discover pioneering safety and health best practices. Our value-added services give our clients a new dimension, which allows for the growth, profitability, stability, and professionalism needed to succeed in today’s marketplace.</p>
                    <p>Atlas’s safety procedures were created to enhance our clients’ accident prevention programs. No matter an organization’s size or length of establishment, any business faced with workplace safety exposures will eventually come under scrutiny by federal, state, and local agencies.</p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="portfolio-item drop-shadow">
                    <div class="portfolio-item-wrap">
                        <div class="portfolio-image">
                            <img src="<?php echo basePathUrl();?>images/Atlas-Services-Safety.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-spacer-30"></div>
        <div class="container">
            <h3>Atlas’ Occupational Safety and Health Management Services Include:</h3>
          <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="list-icon dots list-icon-list list-icon-colored-grey">
                            <li>Accident/Incident Investigation and Analysis</li>
                            <li>Job Hazard Analysis</li>
                            <li>Occupational Safety and Health Program Assessments and Loss Prevention Training</li>
                            <li>Workplace Safety Committee Development</li>
                            <li>Evaluating Ergonomic Risk Factors and Exposures</li>
                            <li>Improving Employee Safety Awareness and Productivity</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="list-icon dots list-icon-list list-icon-colored-grey">
                            <li>Preventing Employee Accidents, Injuries, and Illnesses</li>
                            <li>Reducing Equipment Downtime</li>
                            <li>Reducing the Experience Modification Factor and Insurance Costs</li>
                            <li>Preventing Workers’ Compensation Fraud and Unfavorable Publicity</li>
                            <li>Maintaining Compliance with Regulatory Requirements, Including Safety Programs, and Training</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="section-spacer-20"></div>
        <div class="section-spacer-30"></div>
        <div class="row">
            <div class="col-lg-12">
                <h4 class="text-center">Request a Free HR Diagnostic</h4>
                <p class="m-t-20 text-center">
                    <a href="#modalLetsGetStarted" data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">Let's Get Started</a>
                </p>
                <div class="section-spacer-30"></div>
            </div>
        </div>
    </div>
    
</section>