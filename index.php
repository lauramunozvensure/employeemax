<?php
// global php functions and variables
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require 'include/functions.php';

// this section isn't a website route, it handle the ajax requests used by the forms submits
if ($isFormSend) {
    switch ($requestedPage) {
        case 'form-send/marketplace' :
            require __DIR__ . '/include/marketplace.php';
            break;
        case 'form-send/training' :
            require __DIR__ . '/include/training.php';
            break;
        case 'form-send/customer-service' :
            require __DIR__ . '/include/contact-customer-service.php';
            break;
        case 'form-send/become-agent' :
            require __DIR__ . '/include/contact-become-an-agent.php';
            break;
        case 'form-send/referrals' :
            require __DIR__ . '/include/referrals.php';
            break;
        case 'form-send/hr-download' :
            require __DIR__ . '/include/hr-download.php';
            break;
        case 'form-send/free-diagnostic' :
            require __DIR__ . '/include/free-diagnostic.php';
            break;
        case 'form-send/free-diagnostic-home' :
            require __DIR__ . '/include/free-diagnostic-home.php';
            break;
    }
    exit();
}
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="max-snippet:-1,max-image-preview:large,max-video-preview:-1,index,follow"/>

    <!-- Do not make changes here if you do not know what are you doing - Title and Metas -->
    <?php
        $metaTagsItems = json_decode(file_get_contents('include/metas.json'));
        $filteredObject = filterByKeyValue($metaTagsItems, 'link', $_SERVER['QUERY_STRING']);
        echo '<title>'.$filteredObject[0]->metas->title.'</title>';
        echo '<meta name="author" content="'.$filteredObject[0]->metas->author.'" />';
        if (strlen(trim($filteredObject[0]->metas->description)) > 0) {
            echo '<meta name="description" content="'.$filteredObject[0]->metas->description.'" />';
            echo '<meta property="og:description" content="'.$filteredObject[0]->metas->description.'" />';
            echo '<meta name="twitter:description" content="'.$filteredObject[0]->metas->description.'" />';
        }
        echo '<meta property="og:locale" content="'.$filteredObject[0]->metas->oglocale.'" />';
        echo '<meta property="og:type" content="'.$filteredObject[0]->metas->ogtype.'" />';
        echo '<meta property="og:title" content="'.$filteredObject[0]->metas->title.'" />';
        echo '<meta property="og:url" content="';
        echo getFullUrl().'" />';
        echo '<meta property="og:site_name" content="'.$filteredObject[0]->metas->ogsite_name.'" />';
        echo '<meta property="article:publisher" content="'.$filteredObject[0]->metas->articlepublisher.'" />';
        echo '<meta name="twitter:card" content="'.$filteredObject[0]->metas->twittercard.'" />';
        echo '<meta name="twitter:title" content="'.$filteredObject[0]->metas->title.'" />';
        echo '<meta name="twitter:site" content="'.$filteredObject[0]->metas->twittersite.'" />';
        echo '<meta name="twitter:creator" content="'.$filteredObject[0]->metas->twittercreator.'" />';
        echo '<link rel="canonical" href="';
        echo getFullUrl().'" />';
    ?>
    <!-- end: Title/metas -->
    <link rel="icon" type="image/png" href="<?php echo basePathUrl();?>images/fav-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo basePathUrl();?>images/fav-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" type="image/png" href="<?php echo basePathUrl();?>images/fav-180x180.png">
    <link href="//fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900,300italic,400italic,700italic|Roboto:100,200,300,400,500,600,700,800,900,300italic,400italic,700italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <meta name="msapplication-TileImage" content="<?php echo basePathUrl();?>images/fav-270x270.png" />
    <!-- Css Stylesheets & Fonts -->
    <link href="<?php echo basePathUrl();?>css/plugins.css" rel="stylesheet">
    <link href="<?php echo basePathUrl();?>css/style.css" rel="stylesheet">
    <link href="<?php echo basePathUrl();?>css/theme.css" rel="stylesheet">
    <link href="<?php echo basePathUrl();?>css/custom.css" rel="stylesheet">
    <link href="<?php echo basePathUrl()?>css/map-style.css" rel="stylesheet">
    <link href="<?php echo basePathUrl()?>css/bootstrap-pre-header.css" rel="stylesheet">
    <!-- end: Css Stylesheets & Fonts -->
</head>

<body class="<?php echo ($isPortalsPage) ? 'breakpoint-xl b--desktop' : '';?>">
    

<!-- Client Center map container -->
<span id="usjstip"></span>
<!-- end: Client Center map container -->
<div class="body-inner">
    <?php
    // global header and top section
    if (!$isPortalsPage) {
		
    //extra top nav
    /*
		echo '<div id="topbar" class="d-none d-xl-block d-lg-block topbar-fullwidth">';
		echo '<div class="container container-top-nav">';
		echo '<div class="row">';
		echo '<div class="col-md-7">';
		echo '<ul class="top-menu">';
		echo '<li><a href="/covid19-resources"><b>Vensure COVID-19 Resources</b></a></li>';
		echo '</ul>';
		echo '</div>';
		echo '</div>';
		echo '</div>';
        echo '</div>';
        */
        echo '<div id="ph-pre-header">
            <div class="container-fluid js-preheader banner-cta full-width bg-grey-f3f3f3 blue-close fadeIn">
                <section class="row_preh center-block_preh" >
                    <div class="col-xs-12 text-main p-5">
                        <div class="copy">
                            <div class="col-xs-12 col-sm-3 px-0">
                                <img alt="Medkit" src="https://www.vensure.com/images/orange-medkit.png" style="height: 42px;" class="mr-20 float-left" /><strong class="size20" style="line-height: 28px;">EmployeeMax Coronavirus (COVID-19) Updates</strong>
                            </div>
                            <div class="col-xs-12 col-sm-9 pl-sm-40 px-0">
                                <h5>EmployeeMax is actively monitoring Coronavirus (COVID-19) developments. We have compiled valuable resources for you to utilize as the Coronavirus situation continues to evolve. Learn more about our <a href="/covid19-resources"><font color="#e05206">COVID-19 List of Resources here</font></a>.</h5>
                            </div>
                        </div>
                    </div>      
                    <div class="preheader-close" onclick="parentNode.remove()"></div>          
                </section>
            </div>
        </div>';
        


    //main top nav
        
		echo '<div id="topbar" class="d-none d-xl-block d-lg-block topbar-fullwidth">';
		echo '<div class="container container-top-nav">';
		echo '<div class="row">';
		echo '<div class="col-md-12">';
		require 'include/top-nav.php';
		echo '</div>';
		echo '</div>';
		echo '</div>';
		echo '</div>';
		      
        echo  '<header id="header" data-fullwidth="true"><div class="header-inner"><div class="container"><div id="logo"><a href="' . basePathUrl() . '"><img src="' . basePathUrl() . 'images/employeemax/Employee-Max-logo.png" class="logo-default"></a></div>';
        require 'include/nav.php';
        echo '</div></div></header>';
    }
        // internal content pages
        switch ($_SERVER['QUERY_STRING']) {
            case '/' :
                echo
                require __DIR__ . '/views/home.php';
                break;
            case '' :
                require __DIR__ . '/views/home.php';
                break;
            case 'about' :
            case 'about/' :
                require __DIR__ . '/views/about-us.php';
                break;
            case 'payroll-services' :
            case 'payroll-services/' :
                require __DIR__ . '/views/payroll-services.php';
                break;
            case 'hr-services' :
            case 'hr-services/' :
                require __DIR__ . '/views/hr-services.php';
                break;
            case 'contact' :
            case 'contact/' :
                require __DIR__ . '/views/contact-us.php';
                break;
            case 'blog' :
            case 'blog/' :
                require __DIR__ . '/views/blog.php';
                break;
            case 'faqs' :
            case 'faqs/' :
                require __DIR__ . '/views/faq.php';
                break;
            case 'benefits' :
            case 'benefits/' :
                require __DIR__ . '/views/benefits.php';
                break;
            case 'workers-compensation' :
            case 'workers-compensation/' :
                require __DIR__ . '/views/workers-compensation.php';
                break;
            case 'privacy-policy' :
            case 'privacy-policy/' :
                require __DIR__ . '/views/privacy-policy.php';
                break;
            case 'terms-of-use' :
            case 'terms-of-use/' :
                require __DIR__ . '/views/terms-of-use.php';
                break;
            case 'covid19-resources' :
            case 'covid19-resources/' :
                require __DIR__ . '/views/covid19-resources.php';
                break;
            case 'landing-page/thank-you' :
            case 'landing-page/thank-you/' :
                require __DIR__ . '/views/landing-page-thank-you.php';
                break;
            default:
                http_response_code(404);
                require __DIR__ . '/views/404.php';
                break;
       
        }
        // end: internal Content pages
    if (!$isPortalsPage) {
        // footer section
        require 'include/footer.php';
    }
    ?>
    <div id="modalLetsGetStarted" class="modal no-padding" data-delay="3000" style="max-width: 400px;">
        <div class="p-30 p-xs-20">
            <h2 class="started-modal">Request a Free HR Diagnostic</h2>
            <iframe src="https://go.vensure.com/l/656143/2020-04-13/2bx224" width="90%" height="375" type="text/html" frameborder="0" allowtransparency="true" style="border: 0" scrolling="no"></iframe>
        </div>
    </div>
    <div id="modalBecomeAPartnerToday" class="modal no-padding" data-delay="2000" style="max-width: 400px;">
        <div class="p-30 p-xs-20">
            <h2 class="started-modal">Become a Partner Today</h2>
            <iframe src="https://go.vensure.com/l/656143/2020-01-24/ll619" width="90%" height="480" type="text/html" frameborder="0" allowtransparency="true" style="border: 0" scrolling="no"></iframe>
        </div>
    </div>
    <?php
    // HR insider popup left-bottom corner (only load in the home page)
    if ($isIndex) {
    echo '<div id="hrDownload" class="popup-hr-download modal-bottom modal-auto-open" data-delay="2000">
        <div class="container">
            <div class="row m-b-5">
                <div class="col-md-12 text-right p-0"><button type="button" class="pum-close modal-close">x</button></div>
            </div>
            <div class="row">
                <div class="col-md-3 p-5">
                    <img src="images/Vensure-HR-Insider-Preview.jpg" />
                </div>
                <div class="col-md-9 p-t-0 p-l-10 p-5">
                    Find out how partnering with a PEO saves you time and money while empowering you to grow your business with confidence.
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 p-r-10 p-l-10">
                    <form class="form-popup-hr" novalidate action="';
    echo basePathUrl();
            echo 'form-send/hr-download" role="form" method="post" data-hr-insider="https://go.vensure.com/l/656143/2019-07-01/2xgll/656143/31185/VensureHR_What_is_a_PEO_f_em.pdf">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="hr_email">Your Company Email</label>
                                <input type="email" class="form-control form-input-home email" id="hr_email" name="hr_email" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button type="submit" id="popup-hr-bottom" class="btn btn-rounded btn-light hr-button">Get Your Free HR Insider Now</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>';
    }
    ?>
</div>
<!-- global Javascript Plugins-->
<script src="<?php echo basePathUrl();?>js/jquery.js"></script>
<script src="<?php echo basePathUrl();?>js/plugins.js"></script>
<!-- end: global Javascript Plugins-->
<!--Javascript functions-->
<script src="<?php echo basePathUrl();?>js/functions.js"></script>
<script src="<?php echo basePathUrl();?>js/custom.js"></script>
<!-- end: Javascript functions-->
<!-- client center map -->
<script src="<?php echo basePathUrl()?>js/map-config.js"></script>
<script src="<?php echo basePathUrl()?>js/map-interact.js"></script>
<!-- end: client center map -->
<script src="<?php echo basePathUrl()?>js/pie-chart.js"></script>
<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 10821202;
    (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' === document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
</script>
<noscript>
    <a href="https://www.livechatinc.com/chat-with/10821202/" rel="nofollow">Chat with us</a>,
    powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
</noscript>
<!-- end: LiveChat code -->
</body>
</html>
