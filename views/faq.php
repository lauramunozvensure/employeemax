<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/employeemax-frequently-asked-questions.jpg">
    <div class="container">
        <div class="page-title">
            <h1 class="">Frequently Asked Questions</h1>
            <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="tesxt-justify">Here are some of the most common questions 
                    we are asked and answers our experts have provided. For more 
                    information or if you have a question not listed here, please 
                    <a class="contact" href="<?php echo basePathUrl();?>contact">
                    <b>contact EmployeeMax</b><a> where one 
                    of our PEO specialists can assist you.</p>
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" 
                                data-toggle="collapse" data-target="#collapseOne"
                                 aria-expanded="false" aria-controls="collapseOne"
                                 onmouseover="accordionHover(this)" onmouseout="accordionNoHover(this)">
                                    <p>How do employees get the correct routing and account numbers?</p>
                                    <i class="fa fa-times fa-rotate-45"></i>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse" 
                        aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            They should use the numbers at the bottom of a 
                            regular check, or call their financial institution 
                            to verify numbers for other types of accounts. 
                            Do not use the numbers on a deposit slip.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" 
                                data-toggle="collapse" data-target="#collapseTwo"
                                 aria-expanded="false" aria-controls="collapseTwo"
                                 onmouseover="accordionHover(this)" onmouseout="accordionNoHover(this)">
                                    <p>How does Direct Deposit work?</p>
                                    <i class="fa fa-times fa-rotate-45"></i>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" 
                        aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Your employees provide us with their account numbers 
                            where they would like their money deposited. Their 
                            payroll will be deposited automatically into their 
                            account on the regular payroll check date.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" 
                                data-toggle="collapse" data-target="#collapseThree"
                                 aria-expanded="false" aria-controls="collapseThree"
                                 onmouseover="accordionHover(this)" onmouseout="accordionNoHover(this)">
                                    <p>Do I have to change my bank or my payroll account?</p>
                                    <i class="fa fa-times fa-rotate-45"></i>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" 
                        aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                            Definitely NOT!
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" 
                                data-toggle="collapse" data-target="#collapseFour"
                                 aria-expanded="false" aria-controls="collapseFour"
                                 onmouseover="accordionHover(this)" onmouseout="accordionNoHover(this)">
                                    <p>At what point in the payroll will my payroll account be debited?</p>
                                    <i class="fa fa-times fa-rotate-45"></i>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse" 
                        aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                            Direct Deposit and Taxes will be debited one banking day prior to check date.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" 
                                data-toggle="collapse" data-target="#collapseFive"
                                 aria-expanded="false" aria-controls="collapseFive"
                                 onmouseover="accordionHover(this)" onmouseout="accordionNoHover(this)">
                                    <p>What type of tax services do you provide?</p>
                                    <i class="fa fa-times fa-rotate-45"></i>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" 
                        aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                            We handle all payroll taxes including employee withholdings 
                            as well as employer taxes. We prepare Federal Forms 940 & 941, 
                            all Quarterly Returns, State Unemployment and Withholdings 
                            Returns, as well as all Federal and State W2’s. We also 
                            prepare and file all Local and City taxes.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSix">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" 
                                data-toggle="collapse" data-target="#collapseSix"
                                 aria-expanded="false" aria-controls="collapseSix"
                                 onmouseover="accordionHover(this)" onmouseout="accordionNoHover(this)">
                                    <p>Do you process Multi-State payrolls?</p>
                                    <i class="fa fa-times fa-rotate-45"></i>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseSix" class="collapse" 
                        aria-labelledby="headingSix" data-parent="#accordion">
                            <div class="card-body">
                            Yes. We have clients all across the United States 
                            and many of them have multiple locations. With our 
                            Internet technology, you can also have each location 
                            login and enter their own payroll information as needed.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSeven">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" 
                                data-toggle="collapse" data-target="#collapseSeven"
                                 aria-expanded="false" aria-controls="collapseSeven"
                                 onmouseover="accordionHover(this)" onmouseout="accordionNoHover(this)">
                                    <p>For what size companies does EmployeeMax do payroll?</p>
                                    <i class="fa fa-times fa-rotate-45"></i>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseSeven" class="collapse" 
                        aria-labelledby="headingSeven" data-parent="#accordion">
                            <div class="card-body">
                            We have clients that range in size from one 
                            employee to more than 3000 employees.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function accordionHover(elm){
        elm.className += " hover";
    }
    function accordionNoHover(elm){
        elm.classList.remove("hover");
    }
</script>