
    <section class="fullscreen home" data-bg-parallax-home="<?php echo basePathUrl();?>images/employeemax/parrallax-home.png">
        <div class=""></div>
        <div class="container-wide">
            <div class="container-fullscreen">
                <div class="text-middle">
                    <div class="heading-text no-bottom">
                        <h1><span class="text-white">Welcome to Employee Max</span></h1>
                        <p class="text-white">Integrated PEO Solutions</p>
                            <a class="btn in-message-parallax" href="<?php echo basePathUrl();?>PENDING">Schedule a FREE Demo</a>
                        <!-- <p><a href="#modalLetsGetStarted" 
                        data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">
                        Request a Quote</a>&nbsp;&nbsp;<a href="#modalLetsGetStarted" 
                        data-lightbox="inline" class="btn btn-modal btn-rounded btn-light">
                        Request a Demo</a></p> -->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Outsourcing Your Payroll</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div>
                        <p class="text-justify">EmployeeMax offers a fully integrated platform that provides the most 
                            desired functions for HR and payroll management.  Through a seamless 
                            conversion from your former system and a full integration with our platform,
                             you’ll have access to customizable features, all-inclusive payroll tax reporting,
                              specialized security, and personalized customer service from our software engineers.</p>
                        <div class="inside-spacer"></div>
                        <p class="text-justify">Our team of certified payroll professionals is equipped with over 120 years of 
                            collective experience in multi-state payrolls, human resources, customer service,
                             and accounting, creating the foundation for EmployeeMax’s leadership in secure,
                              cloud-based HR and payroll services.</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/Atlas-Home-Who-We-Are.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="background-gray">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Partner with EmployeeMax</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/Atlas-Home-Who-We-Are.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div>
                        <p class="text-justify">EmployeeMax is your trusted professional 
                            employer organization (PEO) provider, delivering comprehensive, 
                            professional human resources, payroll, benefits, 
                            and workers’ compensation solutions. Our team of seasoned 
                            experts across all departments allows us to provide 
                            integrated technology and software, long-standing business 
                            relationships for industry-leading resources and information, 
                            and excellent customer service support for business owners 
                            of all sizes. We value the trust business owners place in 
                            EmployeeMax’s services, which is why we put all our efforts 
                            into maximizing your business’s success with customized 
                            solutions tailored to your individual business needs.</p>
                        <div class="inside-spacer"></div>
                        <p class="text-justify">Our goal is to make business 
                            processes as simple and seamless as possible so 
                            business owners can reach their business goals faster. 
                            Partnering with EmployeeMax gives you the tools, 
                            resources, and support to run your business more 
                            efficiently and potentially save money and time 
                            investing in in-house hiring, training, and retention 
                            efforts.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
    <div class="container">
    <div class="heading-text heading-section text-center">
                <h4>Our Services</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
        <div class="row">
            <div class="row justify-content-center text-center">
                <div class="col-lg-2">
                    <img src="<?php echo basePathUrl();?>images/employeemax/Payroll-Icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/restaurant"><font color="54575a">Payroll</font></a>
                    </h5>
                    <p class="font-size-16">Integrated, simple, and accurate payroll and tax, saving you money and time.</p>
                    <div class="section-spacer-30"></div>
                </div>
                <div class="col-lg-2">
                    <img src="<?php echo basePathUrl();?>images/employeemax/HR-Icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/healthcare"><font color="54575a">Human Resources</font></a>
                    </h5>
                    <p class="font-size-16">Simple, centralized, and automated systems to eliminate multiple systems and increase accuracy.</p>
                    <div class="section-spacer-30"></div>
                </div>
                <div class="col-lg-2">
                    <img src="<?php echo basePathUrl();?>images/employeemax/Benefits-Icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/tech"><font color="54575a">Employee Benefits</font></a>
                    </h5>
                    <p class="font-size-16">Extensive resources available for adequate employee benefit options.</p>
                    <div class="section-spacer-30"></div>
                </div>
                <div class="col-lg-2">
                    <img src="<?php echo basePathUrl();?>images/employeemax/Time-and-Attendance-Icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/non-profit"><font color="54575a">Time and Attendance</font></a>
                    </h5>
                    <p class="font-size-16">Web-based, biometric support for accurate time and labor tracking.</p>
                    <div class="section-spacer-30"></div>
                </div>
                <div class="col-lg-2">
                    <img src="<?php echo basePathUrl();?>images/employeemax/Workers-Comp-Icon.png"><div class="section-spacer-10"></div>
                    <h5 class="home-services">
                        <a href="<?php echo basePathUrl();?>industry/construction"><font color="54575a">Worker's Compensation</font></a>
                    </h5>
                    <p class="font-size-16">Affordable, pay-as-you-go programs that cover your employees from injury or accident.</p>
                    <div class="section-spacer-30"></div>
                </div>
            </div>
        </div>
    </div>   
    </section>

    <section class="background-gray">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Outsourcing Your Back-Office Services</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                        <p class="text-justify">EmployeeMax offers a fully integrated
                             platform that provides the most desired functions for payroll, 
                             human resources, employee benefits, and workers’ compensation. 
                             Through a seamless conversion from your former system and 
                             complete integration with our platform, you’ll have access 
                             to customizable features, all-inclusive payroll tax reporting, 
                             specialized security, and personalized customer service 
                             from our software engineers.</p>
                             <div class="inside-spacer"></div>
                             <p class="text-justify">Integration with Intacct provides:</p>
                            <div class="inside-spacer"></div>
                            <ul class="cool-list">
                                <li><p>Simple, Straightforward Conversion and Integration</p></li>
                                <li><p>Complete Automation</p></li>
                                <li><p>General Ledger</p> </li>
                                <li><p>Timecards</p> </li>
                                <li><p>Integrated Payroll and Accounting Solutions</p></li>
                            </ul>
                            <div class="inside-spacer"></div>
                            <p class="text-justify">Our team of certified PEO professionals is equipped
                                  with over 120 years of collective experience
                                   in multi-state payrolls, human resources, customer 
                                   service, and accounting, creating the foundation for 
                                   EmployeeMax’s leadership in secure, cloud-based PEO 
                                   services.</p>

                </div>
            </div>
        </div>
    </section>

    <!-- end: Content -->