<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/employeemax-about-us.jpg">
    <div class="container">
        <div class="page-title">
            <h1 class="">Reinventing Payroll and HR</h1>
            <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
        </div>
    </div>
</section>

<section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>About Us</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div>
                        <p class="text-justify">Simplicity, accuracy, and innovation
                             are the core of EmployeeMax business values. Our 
                             seasoned team of payroll professionals lead our 
                             cutting-edge payroll and HR services. Whether it is
                              simplifying technology systems or pioneering customized
                               payroll solutions, EmployeeMax provides a comprehensive
                                suite of services to address any payroll or HR issues
                                 your business experiences.</p>
                        <div class="inside-spacer"></div>
                        <p class="text-justify">With our extensive experience with
                             other payroll and HR systems and software, we aim to
                              develop a platform to achieve four key goals:</p>
                        <div class="inside-spacer"></div>
                       
                            <ul class="cool-list">
                                <li><p>Reinvent Payroll Processes</p></li>
                                <li><p>Simplify Customer Experience</p></li>
                                <li><p>Deliver Secure Access</p></li>
                                <li><p>Establish Reliable Products and Services</p></li>
                            </ul>
                            
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>The EmployeeMax Difference</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <span class="lead sub-header"></span>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div>
                        <div>
                        <h3>Premium Service</h3>
                        <p class="text-justify">Receive informed, friendly, one-on-one customer
                             service from our in-house team of Certified Payroll Professional (CPP) 
                             experts. As creators of the platform, they are positioned to answer 
                             your questions quickly and effectively.</p>
                        </div>
                        <div class="inside-spacer"></div>
                        <div>
                        <h3>Simplified Billing</h3>
                        <p class="text-justify">Enjoy significant annual savings compared
                             to your previous provider, without having to worry about a 
                             surprise bills, hidden charges, or processing fees. You get
                              one monthly bill at flat monthly rate for everything included 
                              in the package.</p>
                        </div>
                        <div class="inside-spacer"></div>
                        <div>
                        <h3>Easy-to-Use System</h3>
                        <p class="text-justify">Easy Use & Easy Access – Our cloud-based 
                            EmployeeMax platform was designed with simplicity in mind.
                             Its straightforward interface and flexibility make it very easy
                              to learn. We handle of the transition from your old system 
                              safely and securely. Employee data, year-to-dates, tax filing
                               accounts, direct deposits, and parallel payrolls will be
                                transfer to our platform for you.  You can enjoy the flexibility
                                of viewing and printing your data anytime with Internet access.
                                Additionally, free training, demos, webinars, and expert
                                customer service are at your disposal.</p>
                        </div>
                        <div class="inside-spacer"></div>
                        <div>
                        <h3>Custom Reporting</h3>
                        <p class="text-justify">Custom Reporting – Every business is unique,
                             so we don’t use a one-size-fits-all reporting solution.
                              We create all your business’s payroll reports according
                               to your needs. You can view and print these reports
                                as well as all your other data anytime, anywhere with
                                 Internet access.</p>
                        </div>
                        <div class="inside-spacer"></div>
                        <div>
                        <h3>Comprehensive PEO Services</h3>
                        <p class="text-justify">Our in-house team of Certified Payroll
                             Professionals (CPP) take care of everything. We prepare
                              and file all state and federal payroll taxes to the 
                              designated agencies and send you copies on a quarterly 
                              basis. We file all of your W2s, W3s and annual 940s, 
                              and even produce your 1099 forms for you to file. If an 
                              agency ever contacts you with a tax notice, just forward 
                              the document to us and go back to running your business;
                               you won’t need to worry about payroll taxes.</p>
                        </div>
                        <div class="inside-spacer"></div>
                        <div>
                        <h3>Free, Seamless Integrations</h3>
                        <p class="text-justify">Our system is feature-packed to fully
                             integrate with your financial software, time and attendance
                              systems, and all other aspects of your business. 
                              This means less time, effort, and money dedicated 
                              to researching, selecting, and purchasing separate modules
                               for feature-by-feature integration. With EmployeeMax,
                                integrations are seamless and free.</p>
                        </div>
                        <div class="inside-spacer"></div>
                        <div>
                        <h3>Robust Business Security</h3>
                        <p class="text-justify">Your security needs are unique. We give you
                             limited access to fields for as many users as you like according 
                             to your individual security needs. At the same time, we put your
                              data on a secure network, so you never have to create your own 
                              backup or risk losing information.</p>
                        <p class="text-justify">EmployeeMax University is a unique online
                             forum where clients are able to increase knowledge of 
                             best practices, continuing education on updates to laws 
                             and regulations relevant to the industry, and <a class="highlighted"
                              href="<?php echo basePathUrl();?>PENDING">workshops</a> 
                             to enhance skill-sets to strengthen business efficiencies.</p>     
                        
                        </div>
                        <div class="inside-spacer"></div>  
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/EmployeeMax-CTA-Meeting-Background.jpg">
        <div class="container d-flex">
            <div class="message-parallax">
                <h2>Have Questions About EmployeeMax?</h2>
                <h3>See EmployeeMax in Action</h3>
                <div class="inside-spacer"></div>
                <a class="btn" href="<?php echo basePathUrl();?>PENDING">Schedule a FREE Demo</a>
            </div>
        </div>
    </section>