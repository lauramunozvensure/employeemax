<section id="page-title" class="internals contact" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/EmployeeMax-Contact.jpg">
    <div class="container">
        <div class="page-title">
            <h1 class="">Contact EmployeeMax</h1>
            <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
        </div>
    </div>
</section>

<section id="page-content" class="sidebar-right">
    <div class="section-spacer-40"></div>
    <div class="container">
        <div class="row">
            <div class="content col-lg-8">
                <h3 class="highlighted">Current Clients: This form is not intended for 
                    current client processing or service inquiries. 
                    If you are a current client, please reach out to
                     your Client Relations representative.</h3>
                <div class="section-spacer-20"></div>
                <iframe src="https://go.vensure.com/l/656143/2020-04-13/2bx3g8" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
                </div>
            <!-- Sidebar -->
            <div class="sidebar contact col-lg-4">
                <div class="background-light sidebar">
                    <div class="section-spacer-10"></div>
                <div class="section-spacer-8"></div>
                <p><b>Contact us to learn more</b> or schedule a demonstration</p> 
                
                <div class="icon-box1 medium color">
                    <div class="icon"><i class="fas fa-phone fa-rotate-90 contact"></i></div>
                    <h5 class="training-calendar contact">Tel: 888-376-7291</h5>
                </div>
                <div class="section-spacer-8"></div>
                <div class="icon-box1 medium color">
                    <div class="icon"><i class="fas fa-phone fa-rotate-90 contact"></i></div>
                    <h5 class="training-calendar contact">Customer Service: 1-888-376-7291</h5>
                </div>
                <div class="section-spacer-8"></div>
                <div class="icon-box1 medium color">
                    <div class="icon"><i class="fas fa-envelope contact"></i></div>
                    <h5 class="training-calendar contact">service@employeemax.com</h5>
                </div>
                <div class="section-spacer-10"></div>
            </div>
            </div>
        </div>
</section>