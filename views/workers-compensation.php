<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/employeemax-payroll.jpg">
    <div class="container">
        <div class="page-title">
            <h1 class="">Workers'Compensation</h1>
            <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
        </div>
    </div>
</section>
<section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Protect Your Employees from Workplace Injuries and Accidents</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div>
                        <p class="text-justify">More than ever, employers need real 
                            solutions to help them reduce the cost of insurance 
                            coverage and improve business cash flow. Workers’ 
                            compensation provides both employers and employees 
                            protection for work-related injuries and accidents. </p>
                        <div class="inside-spacer"></div>
                        <p class="text-justify">Our team of certified payroll professionals is equipped with over 120 years of 
                            collective experience in multi-state payrolls, human resources, customer service,
                             and accounting, creating the foundation for EmployeeMax’s leadership in secure,
                              cloud-based HR and payroll services.</p>
                        <div class="inside-spacer"></div>
                        <p>Benefits of Workers’ Compensation, include:</p>
                        <div class="inside-spacer"></div>
                        <ul class="cool-list">
                                <li><p>Eliminates big premium deposits</p></li>
                                <li><p>Reduces your audit risk</p></li>
                                <li><p>Improves business cash flow</p></li>
                            </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Suite of Full-Service Workers’ Compensation Solutions</h4>
                <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div>
                        <p class="text-justify">Pay-as-you-go workers’ compensation insurance 
                            enables businesses to buy workers’ compensation with little or no 
                            money down. Our exclusive Pay-As-You-Go programs also help protect 
                            employers from large audit bills because the premium is based on 
                            real-time payroll wages and reporting. We are affiliated with most 
                            of the top insurance carriers in the United States, which means we 
                            can write a policy for most risks and industries.</p>
                        <div class="inside-spacer"></div>
                        <p class="text-justify">From developing safety manuals and providing 
                            OSHA training to conducting onsite inspections and presenting 
                            opportunities for open market plans, EmployeeMax’s workers’ 
                            compensation team is readily equipped to mitigate risk to 
                            safeguard worksite employees’ safety and compliance. Our risk 
                            and safety experts are available to assist with any questions 
                            or concerns you may have regarding workers’ compensation, 
                            compliance, and other risk and safety-related issues.</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/Atlas-Home-Who-We-Are.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/EmployeeMax-CTA-Meeting-Background.jpg">
        <div class="container d-flex">
            <div class="message-parallax">
                <h2>Have Questions About EmployeeMax?</h2>
                <h3>See EmployeeMax in Action</h3>
                <div class="inside-spacer"></div>
                <a class="btn" href="<?php echo basePathUrl();?>PENDING">Schedule a FREE Demo</a>
            </div>
        </div>
    </section>
