<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/EmployeeMax-HRServices.jpg">
    <div class="container">
        <div class="page-title">
            <h1 class="">HR Services</h1>
            <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
            <h5>Manage All Your HR Services with One Tool</h5>
        </div>
    </div>
</section>

<section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4 class="highlighted">HR Features</h4>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div>
                        <p class="text-justify">EmployeeMax’s human resource
                             services are integrated in one platform with payroll.
                               Combining two systems into a single program saves
                                you from manually entering information into multiple systems.
                                  Like the payroll system, our human resource software
                                   is customizable and can fully synchronize with all
                                    other aspects of your business.</p>
                        <div class="inside-spacer"></div>                     
                          <p class="text-justify">EmployeeMax offers a variety of HR services, from employee
                               benefits and time tracking to pay-as-you-go workers’
                                compensation and HR consulting.</p>
                            
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="background-gray">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4 class="highlighted">HRIS</h4>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h5 class="title">Protect Yourself from Costly Payroll Errors</h5>
                    <div class="section-spacer-10"></div>
                    <p>Our system was built to provide growing businesses a turnkey
                     solution that goes beyond payroll and HR to include benefits management.</p>
                    <div class="section-spacer-10"></div>
                    <p class="text-justify">Human Resource Information Systems (HRIS) have become one of
                     the most important tools for many businesses. Even small
                      offices can benefit by using HRIS to become more efficient.
                       Many firms don’t realize how much time and money is 
                       wasted on manual human resource management (HRM)
                        tasks until they switch to an HRIS.</p>
                    <div class="section-spacer-10"></div>
                    <p class="text-justify">HRIS are secure and web-based to provide extensive
                     automation of all HR-related activities.
                      EmployeeMax can offer you the most comprehensive
                       and affordable solutions on the market today.</p>
                    <div class="section-spacer-10"></div>   
                    <p >With EmployeeMax, you:</p>                         
                    <div class="section-spacer-10"></div> 
                    <ul class="cool-list">
                        <li><p>Have a quick and easy implementation</p></li>
                        <li><p>Save money on hardware or software investments</p></li>
                        <li><p>Centralize all HR and benefits data into a single system</p></li>
                        <li><p>Receive powerful reporting and exporting capabilities</p></li>
                        <li><p>Simplify operational complexity and reduce data collection</p></li>
                        <li><p>Automatically and electronically deliver benefits information to each carrier and payroll provider</p></li>
                        <li><p>Eliminate manual management of data across multiple systems</p></li>
                        <li><p>Increase data accuracy and streamline management and communication of information</p></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4 class="highlighted">Employee Portal</h4>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-justify">EmployeeMax’s Employee Portal makes it convenient for your
                         employees to manage their benefits and other HR-related
                          information in real time through a central cloud-based
                           system that is accessible anywhere.  The Employee Portal
                            is easily configured to include the features and content
                             most relevant to your company.</p>
                    <div class="section-spacer-10"></div>
                    <p >Employees can:</p>                         
                    <div class="section-spacer-10"></div> 
                    <ul class="cool-list">
                        <li><p >Easily enroll in and/or change all aspects of their
                             benefits and other HR-related information, which allows
                              them to compare, analyze, and review costs prior to 
                              benefits enrollment.</p></li>
                        <li><p>View plans from different benefit providers and a 
                            consolidated benefits summary report that includes the 
                            coverage they selected with the associated costs.</p></li>
                        <li><p>Explore the value of their compensation package with
                             personalized total compensation statements.</p></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="background-gray">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4 class="highlighted">Industry-Leading Human Resource Solutions</h4>
                <div class="section-spacer-10"></div> 
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-justify">As a professional employer organization (PEO),
                         EmployeeMax undertakes the administrative day-to-day employee
                          related tasks that absorb your valuable time from building
                           your business. A PEO offers a co-employment arrangement
                            where the PEO (EmployeeMax) is the employer on paper,
                             but the client (you) remains the executive decision-maker
                              of your company. This allows EmployeeMax to assist
                               with time-consuming back-office duties so you can
                                refocus your energy on continuing your business’s success.</p>
                    <div class="section-spacer-10"></div>
                    <p class="text-justify">EmployeeMax’s human resources services are
                         integrated in one platform with payroll.  Combining your
                          systems into a single program saves you from manually
                           entering information into multiple systems and helps
                            eliminate potential errors.  Like the payroll system,
                             our human resource software is customizable and can
                              fully synchronize with all other aspects of your business.
                               EmployeeMax offers a variety of HR services, such
                                as time and attendance, applicant tracking, HR 
                                consulting, developing employee handbooks, and much more.</p>                                           
                </div>
            </div>
        </div>
    </section>
    <section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/EmployeeMax-CTA-Meeting-Background.jpg">
        <div class="container d-flex">
            <div class="message-parallax">
                <h2>Have Questions About EmployeeMax?</h2>
                <h3>See EmployeeMax in Action</h3>
                <div class="inside-spacer"></div>
                <a class="btn" href="<?php echo basePathUrl();?>PENDING">Schedule a FREE Demo</a>
            </div>
        </div>
    </section>