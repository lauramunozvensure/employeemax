<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/employeemax-payroll.jpg">
    <div class="container">
        <div class="page-title">
            <h1 class="">Payroll Services</h1>
            <div class="separator  small center  " style="margin-top: 16px;margin-bottom: 16px;background-color: #e05206;height: 3px;width: 64px;"></div>
            <h5>Direct integrations with Intacct creates smoother payroll process</h5>
        </div>
    </div>
</section>

<section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4 class="highlighted">Payroll Features</h4>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div>
                        <p class="text-justify">EmployeeMax’s payroll software,
                             Intacct, has simplified payroll processing so you 
                             can work smarter. Intacct can customize requirements
                              of virtually any business, providing your business
                               with the power and flexibility it deserves.</p>
                        <div class="inside-spacer"></div>                     
                            <ul class="cool-list">
                                <li>
                                <p>Variety of fund distribution options 
                                    (direct deposit, payroll debit card, physical check)</p>
                                </li>
                                <li>
                                <p>On-site paycheck printing</p>
                                </li>
                                <li>
                                <p>Employee self-service portal provides unlimited online
                                     access to paystubs, W-2s, and 1099s</p>
                                </li>
                                <li>
                                <p>Preview payroll to ensure accuracy</p>
                                </li>
                            </ul>
                            
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="background-gray">
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4>Outsourcing Your Payroll Processing Has Many Advantages</h4>
                <div class="section-spacer-10"></div>
            </div>
            <div class="row">
            <div class="col-lg-6 d-flex">
                    <div class="col-lg-6">
                        <h3 class="title">Protect Yourself from Costly Payroll Errors</h3>
                        <p class="text-justify">The IRS estimates that one out of 
                            every three employers makes mistakes in computing 
                            payroll figures. These human errors can lead to stiff
                             financial penalties – something small and medium-sized
                              businesses just can’t afford.</p>                         
                    </div>
                    <div class="col-lg-6 d-flex justify-content-center">
                        <div class="text-center">
                            <figure class="chart paused" data-percent="33">
                                <figcaption><span></span>%</figcaption>
                                <svg width="200" height="200">
                                    <circle class="outer" cx="95" cy="95" r="85" transform="rotate(-90, 95, 95)"/>
                                </svg>
                            </figure> 
                            <h5 class="title">FACT: 33% of Employers Make Payroll Errors</h5> 
                        </div>
                                        
                    </div>
            </div>
            <div class="col-lg-6">
                    <div>
                        <h2>Make Business More Efficient</h2>
                        <p class="text-justify">It’s a full-time job keeping up with the
                             continuously changing tax laws implemented by states. Harsher
                              regulations and steeper penalties even for first-time defaulters
                               has placed heightened responsibilities on payroll processers.</p>
                        <div class="inside-spacer"></div>
                        <p class="text-justify">Outsourcing your payroll processing to experts
                             who are well-informed of tax laws can alleviate your unique
                              payroll needs. EmployeeMax’s team of payroll specialists can
                               assist you with your payroll processing so you can refocus
                                on running your business.</p>                           
                    </div>
            </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <div class="section-spacer-10"></div>
                <h4 class="highlighted">Intacct</h4>
                <div class="section-spacer-10"></div>
                <p>Improve Company Performance Through Integrating Payroll and HR.</p>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="h4 text-center">Seamless Integration with Intacct’s Accounting and Management Platform</h4>
                    <div class="section-spacer-10"></div>     
                </div>
                <div class="col-lg-6 d-flex align-items-center">
                    <div>
                        <div class="item">
                            <div class="icon"><i class="fas fa-chart-line"></i></div>
                            <div class="text">
                                <h5 class="title">Real-time Business Visibility</h5>
                                <p>Designated staff members can access your 
                                    accounting dashboard securely from anywhere, anytime.</p>
                            </div>
                        </div>
                        <div class="section-spacer-10"></div> 
                        <div class="item">
                            <div class="icon"><i class="fas fa-file-signature"></i></div>
                            <div class="text">
                                <h5 class="title">Streamline and Simplify Reporting and Analysis</h5>
                                <p>Intacct makes financial consolidation, reporting, 
                                    and analysis easy across multiple business entities.</p>
                            </div>
                        </div>
                        <div class="section-spacer-10"></div>
                        <div class="item">
                            <div class="icon"><i class="fas fa-robot"></i></div>
                            <div class="text">
                                <h5 class="title">Automate Business Processes</h5>
                                <p>Reduce errors, eliminate routine tasks, and accelerate your closing process.</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-6 d-flex align-items-center">
                    <div class="portfolio-item drop-shadow">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image">
                                <a href="#"><img src="<?php echo basePathUrl();?>images/employeemax/employee-max-video.png" alt=" "></a>
                            </div>
                            <div class="portfolio-description">
                                <a data-lightbox="iframe" href="https://vimeo.com/124315733?autoplay=1">
                                    <i class="fas fa-play"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="h4 text-center">Benefits of Intacct</h4>
                    <div class="section-spacer-10"></div> 
                    <ul class="cool-list">
                                <li><p>Seamless Conversion</p></li>
                                <li><p>Seamless Integration</p></li>
                                <li><p>Complete Automation</p></li>
                                <li><p>General Ledger</p></li>
                                <li><p>Timecards</p></li>
                                <li><p>Total Solutions</p></li>
                            </ul>
                            <div class="section-spacer-10"></div> 
                            <p>Any employer with 50 or more full time employees needs to provide
                                 a 1095-C to each employee starting with 2015 and file a 1094-C
                                  Summary form with the IRS.</p>
                            <div class="section-spacer-10"></div> 
                            <p>EmployeeMax offers you the benefit of having us do your 1095-C
                                 and 1094-C forms. We have added a new screen for employee 
                                 data and modified the dependent screen to aid in the population
                                  of these forms. You can begin populating these screens now, 
                                  or simply wait until year-end. We will send you an Excel 
                                  spreadsheet that you can populate, and we will load the data
                                   for you!</p>
                </div>
            </div>
        </div>
    </section>

<section id="page-title" class="internals" data-bg-parallax="<?php echo basePathUrl();?>images/employeemax/EmployeeMax-CTA-Meeting-Background.jpg">
        <div class="container d-flex">
            <div class="message-parallax">
                <h2>Have Questions About EmployeeMax?</h2>
                <h3>See EmployeeMax in Action</h3>
                <div class="inside-spacer"></div>
                <a class="btn" href="<?php echo basePathUrl();?>PENDING">Schedule a FREE Demo</a>
            </div>
        </div>
    </section>