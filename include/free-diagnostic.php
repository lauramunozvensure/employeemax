<?php
session_cache_limiter('nocache');
header('Expires: ' . gmdate('r', 0));
header('Content-type: application/json');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'php-mailer/src/Exception.php';
require 'php-mailer/src/PHPMailer.php';

$mail = new PHPMailer();

// Form Fields
$full_name = isset($_POST["full_name"]) ? $_POST["full_name"] : null;
$phone_number = isset($_POST["phone_number"]) ? $_POST["phone_number"] : null;
$company = isset($_POST["company"]) ? $_POST["company"] : null;
$number_employees = isset($_POST["number_employees"]) ? $_POST["number_employees"] : null;

// Enter your email address. If you need multiple email recipes simply add a comma: email@domain.com, email2@domain.com
$toEmails = "Lauren.Singletary@vensure.com,William.Kenimer@vensure.com,Hyun.Kim@vensure.com,Shawn.Premo@vensure.com,Julie.dower@vensure.com,Donnie.thomas@vensure.com,meghan.shah@vensure.com,ben.waring@vensure.com";
$fromName = "Vensure Website";
$fromEmail = "vensure@vensure.com";
$subject = "Home Form Fill"; // email subject
$messageContent = ""; // content message

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($full_name != '') {

        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';

        $mail->From = $fromEmail;
        $mail->FromName = $fromName;

        if (strpos($toEmails, ',') !== false) {
            $email_addresses = explode(',', $toEmails);
            foreach ($email_addresses as $email_address) {
                $mail->addAddress(trim($email_address));
            }
        } else {
            $mail->addAddress($toEmails);
        }

        $mail->Subject = $subject;

        $full_name = isset($full_name) ? "Full Name: $full_name<br>" : '';
        $phone_number = isset($phone_number) ? "Phone Number: $phone_number<br>" : '';
        $company = isset($company) ? "Company: $company<br>" : '';
        $number_employees = isset($number_employees) ? "Number of Employees: $number_employees<br>" : '';

        $mail->Body = $messageContent . $full_name . $phone_number . $company . $number_employees;

        if (!$mail->send()) {
            $response = array('response' => 'error', 'message' => $mail->ErrorInfo);
        } else {
            $response = array('response' => 'success');
        }

        echo json_encode($response);
    } else {
        $response = array('response' => 'error');
        echo json_encode($response);
    }

}
