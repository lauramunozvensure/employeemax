<?php
session_cache_limiter('nocache');
header('Expires: ' . gmdate('r', 0));
header('Content-type: application/json');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'php-mailer/src/Exception.php';
require 'php-mailer/src/PHPMailer.php';

$mail = new PHPMailer();

// Form Fields
$first_name = isset($_POST["first_name"]) ? $_POST["first_name"] : null;
$last_name = isset($_POST["last_name"]) ? $_POST["last_name"] : null;
$full_name = (isset($first_name) && isset($last_name)) ? $first_name . ' ' . $last_name : null;
$class = isset($_POST["class"]) ? $_POST["class"] : null;
$company_email = isset($_POST["company_email"]) ? $_POST["company_email"] : null;
$phone_number = isset($_POST["phone_number"]) ? $_POST["phone_number"] : null;
$vensure_client_name = isset($_POST["vensure_client_name"]) ? $_POST["vensure_client_name"] : null;
$client_id = isset($_POST["client_id"]) ? $_POST["client_id"] : null;
$company_name = isset($_POST["company_name"]) ? $_POST["company_name"] : null;
$address = isset($_POST["address"]) ? $_POST["address"] : null;
$city = isset($_POST["city"]) ? $_POST["city"] : null;
$state = isset($_POST["state"]) ? $_POST["state"] : null;
$zip = isset($_POST["zip"]) ? $_POST["zip"] : null;
$training_type = isset($_POST["training_type"]) ? $_POST["training_type"] : null;
$training_type_send = $training_type;

if (isset($training_type)) {
    $training_type_send = ucwords(implode(' ', explode('_', $training_type)));
}

// Enter your email address. If you need multiple email recipes simply add a comma: email@domain.com, email2@domain.com
$toEmails = "asklosscontrol@vensure.com";
$fromName = $full_name;
$fromEmail = "vensure@vensure.com";
$subject = $training_type_send . " Training"; // email subject
$messageContent = ""; // content message

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($company_email != '') {

        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';

        $mail->From = $fromEmail;
        $mail->FromName = $fromName;

        if (strpos($toEmails, ',') !== false) {
            $email_addresses = explode(',', $toEmails);
            foreach ($email_addresses as $email_address) {
                $mail->addAddress(trim($email_address));
            }
        } else {
            $mail->addAddress($toEmails);
        }

        $mail->addReplyTo($company_email, $full_name);
        $mail->Subject = $subject;

        $full_name_send = isset($full_name) ? "Full Name: $full_name<br>" : '';
        $class_send = isset($class) ? "Class: $class<br>" : '';
        $company_email_send = isset($company_email) ? "Company Email: $company_email<br>" : '';
        $phone_number = isset($phone_number) ? "Phone Number: $phone_number<br>" : '';
        $vensure_client_name = isset($vensure_client_name) ? "Vensure Client Name: $vensure_client_name<br>" : '';
        $client_id = isset($client_id) ? "Client Id: $client_id<br>" : '';
        $company_name = isset($company_name) ? "Company Name: $company_name<br>" : '';
        $address = isset($address) ? "Address: $address<br>" : '';
        $city = isset($city) ? "City: $city<br>" : '';
        $state = isset($state) ? "State: $state<br>" : '';
        $zip = isset($zip) ? "Zip: $zip<br>" : '';

        $mail->Body = $messageContent . $full_name_send . $class_send . $company_email_send . $phone_number . $vensure_client_name . $client_id . $company_name . $address . $city . $state . $zip;

        if (!$mail->send()) {
            $response = array('response' => 'error', 'message' => $mail->ErrorInfo);
        } else {
            $response = array('response' => 'success');

            $autoRespondEmail = new PHPMailer();

            $autoRespondEmail->isHTML(true);
            $autoRespondEmail->CharSet = 'UTF-8';
            $autoRespondEmail->From = "vensure@vensure.com";
            $autoRespondEmail->FromName = "Vensure Employer Services";
            $autoRespondEmail->addAddress($company_email);
            $autoRespondEmail->Subject = "Your OSHA Class Registration";
            $autoResponseMessage = "<p>$full_name,</p>";
            $autoResponseMessage .= "<p>Thank you for registering for a $training_type_send class in $class.</p>";
            $autoResponseMessage .= "<p>If you are unable to attend, training session cancellations must be received no later than seven business days prior to the class start date. Employees who no-show or submit a cancellation less than seven days prior to the start date will be charged a $125 fee.<br>";
            $autoResponseMessage .= "<p>To cancel your reservation, please email oshacancellation@vensure.com. You will need to provide your full name, email address, class date and time, and Client ID. </p>";
            $autoResponseMessage .= "<p>To view additional OSHA training sessions, please visit www.vensure.com.</p>";
            $autoResponseMessage .= "<p>Questions about this or future OSHA training sessions? Contact us at losscontrol@vensure.com.</p>";
            $autoResponseMessage .= "<p>We look forward to seeing you in class!</p>";
            $autoResponseMessage .= "<p>Sincerely,</p>";
            $autoResponseMessage .= "<p>Loss Control Team<br>Vensure Employer Services</p>";
            $autoResponseMessage .= "<p>**DO NOT REPLY TO THIS EMAIL**</p>";
            $autoRespondEmail->Body = $autoResponseMessage;
            $autoRespondEmail->send();
        }

        echo json_encode($response);
    } else {
        $response = array('response' => 'error');
        echo json_encode($response);
    }

}
