<!--Navigation Trigger-->
<div id="mainMenu-trigger">
    <a class="lines-button x"><span class="lines"></span></a>
</div>
<!--end: Navigation Trigger-->
<div id="mainMenu">
    <div class="container">
        <nav>
            <ul>
                <?php
                // build the main menu from a json object
                $menuItems = json_decode(file_get_contents('include/menu.json'));
                function buildMenu($menu) {
                    $html = '';
                    foreach ($menu as $item => $menuValue) {
                        $activeMenuClass = '';
                        if (in_array($_SERVER['QUERY_STRING'], $menuValue->actives)) {
                            $activeMenuClass = 'current';
                        }
                        if (isset($menuValue->children) && count($menuValue->children) > 0) {
                            $html .= '<li class="dropdown '.$activeMenuClass.'"><a href="';
                            $html .= basePathUrl() . $menuValue->link .'">'.$menuValue->title.'</a>';
                            $html .= '<ul class="dropdown-menu">';
                            foreach ($menuValue->children as $itemSubMenu => $subMenuValue) {
                                if (isset($subMenuValue->children) && count($subMenuValue->children) > 0) {
                                    $html .= '<li class="dropdown-submenu"><a href="';
                                    $html .= basePathUrl() . $subMenuValue->link.'">'.$subMenuValue->title.'</a>';
                                    $html .= '<ul class="dropdown-menu">';
                                    foreach($subMenuValue->children as $itemSubMenuSub => $subMenuValueSub) {
                                        $html .= '<li><a href="';
                                        $html .= basePathUrl() . $subMenuValueSub->link .'">'.$subMenuValueSub->title.'</a></li>';
                                    }
                                    $html .= '</ul>';
                                    $html .= '</li>';
                                } else {
                                    $html .= '<li><a href="';
                                    $html .= basePathUrl() . $subMenuValue->link.'">'.$subMenuValue->title.'</a></li>';
                                }
                            }
                            $html .= '</ul>';
                            $html .= '</li>';
                        } else {
                            $html .= '<li class="'.$activeMenuClass.'"><a href="';
                            $html .= basePathUrl() . $menuValue->link.'">'.$menuValue->title.'</a></li>';
                        }
                    }
                    return $html;
                }

                echo buildMenu($menuItems);
                ?>
            </ul>
        </nav>
    </div>
</div>
