<?php
session_cache_limiter('nocache');
header('Expires: ' . gmdate('r', 0));
header('Content-type: application/json');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'php-mailer/src/Exception.php';
require 'php-mailer/src/PHPMailer.php';

$marketCompaniesList = [
    'core_id_services' => 'salesdevelopment@vensure.com,lauren.singletary@vensure.com,william.kenimer@vensure.com,hyun.kim@vensure.com,Julie.dower@vensure.com,Donnie.thomas@vensure.com,meghan.shah@vensure.com,ben.waring@vensure.com',
    'md_live' => 'shawn.premo@vensure.com,carlos.tenorio@vensure.com,Mattie.GailPhillips@vensure.com,hyun.kim@vensure.com,lauren.singletary@vensure.com,william.kenimer@vensure.com,Julie.dower@vensure.com,Donnie.thomas@vensure.com,meghan.shah@vensure.com,ben.waring@vensure.com',
    'money_networks' => 'shawn.premo@vensure.com,carlos.tenorio@vensure.com,Mattie.GailPhillips@vensure.com,hyun.kim@vensure.com,lauren.singletary@vensure.com,william.kenimer@vensure.com,Julie.dower@vensure.com,Donnie.thomas@vensure.com,meghan.shah@vensure.com,ben.waring@vensure.com',
    'nationwide' => 'salesdevelopment@vensure.com,lauren.singletary@vensure.com,william.kenimer@vensure.com,hyun.kim@vensure.com,Julie.dower@vensure.com,meghan.shah@vensure.com,ben.waring@vensure.com',
    'proactive_health_management_plan' => 'shawn.premo@vensure.com,carlos.tenorio@vensure.com,Mattie.GailPhillips@vensure.com,hyun.kim@vensure.com,lauren.singletary@vensure.com,william.kenimer@vensure.com,Julie.dower@vensure.com,Donnie.thomas@vensure.com,meghan.shah@vensure.com,ben.waring@vensure.com',
    'solvo' => 'shawn.premo@vensure.com,carlos.tenorio@vensure.com,Mattie.GailPhillips@vensure.com,hyun.kim@vensure.com,lauren.singletary@vensure.com,william.kenimer@vensure.com,Julie.dower@vensure.com,Donnie.thomas@vensure.com,meghan.shah@vensure.com,ben.waring@vensure.com',
    'vensure_employer_service' => 'shawn.premo@vensure.com,carlos.tenorio@vensure.com,Mattie.GailPhillips@vensure.com,hyun.kim@vensure.com,lauren.singletary@vensure.com,william.kenimer@vensure.com,Julie.dower@vensure.com,Donnie.thomas@vensure.com,meghan.shah@vensure.com,ben.waring@vensure.com',
    'zay_zoon' => 'shawn.premo@vensure.com,carlos.tenorio@vensure.com,Mattie.GailPhillips@vensure.com,hyun.kim@vensure.com,lauren.singletary@vensure.com,william.kenimer@vensure.com,Julie.dower@vensure.com,Donnie.thomas@vensure.com,meghan.shah@vensure.com,ben.waring@vensure.com'
];

$mail = new PHPMailer();

// Form Fields
$full_name = isset($_POST["full_name"]) ? $_POST["full_name"] : null;
$company_email = isset($_POST["company_email"]) ? $_POST["company_email"] : null;
$phone_number = isset($_POST["phone_number"]) ? $_POST["phone_number"] : null;
$company = isset($_POST["company"]) ? $_POST["company"] : null;
$number_employees = isset($_POST["number_employees"]) ? $_POST["number_employees"] : null;
$message = isset($_POST["message"]) ? $_POST["message"] : null;
$marketplace_company = isset($_POST["marketplace_company"]) ? $_POST["marketplace_company"] : null;

// Enter your email address. If you need multiple email recipes simply add a comma: email@domain.com, email2@domain.com
$toEmails = '';

if (isset($marketplace_company)) {
    $marketplaceCompanyName = strtoupper(implode(' ', explode('_', $marketplace_company)));
    if (array_key_exists($marketplace_company, $marketCompaniesList)) {
        $toEmails = $marketCompaniesList[$marketplace_company];
    }
}

$fromName = "Vensure Website";
$fromEmail = "vensure@vensure.com";
$subject = "Marketplace - " .  $marketplaceCompanyName; // email subject
$messageContent = ""; // content message

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($company_email != '') {

        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';

        $mail->From = $fromEmail;
        $mail->FromName = $fromName;

        if (strpos($toEmails, ',') !== false) {
            $email_addresses = explode(',', $toEmails);
            foreach ($email_addresses as $email_address) {
                $mail->addAddress(trim($email_address));
            }
        } else {
            $mail->addAddress($toEmails);
        }

        $mail->addReplyTo($company_email, $full_name);
        $mail->Subject = $subject;

        $full_name = isset($full_name) ? "Full Name: $full_name<br>" : '';
        $company_email = isset($company_email) ? "Company Email: $company_email<br>" : '';
        $phone_number = isset($phone_number) ? "Phone Number: $phone_number<br>" : '';
        $company = isset($company) ? "Company: $company<br>" : '';
        $number_employees = isset($number_employees) ? "Number of Employees: $number_employees<br>" : '';
        $message = isset($message) ? "Message: $message<br>" : '';

        $mail->Body = $messageContent . $full_name . $company_email . $phone_number . $company . $number_employees . $message;

        if (!$mail->send()) {
            $response = array('response' => 'error', 'message' => $mail->ErrorInfo);
        } else {
            $response = array('response' => 'success');
        }

        echo json_encode($response);
    } else {
        $response = array('response' => 'error');
        echo json_encode($response);
    }

}
