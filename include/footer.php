<footer id="footer">
    <div class="footer-content">
        <div class="container">
            <div class="row gap-y">
                <div class="col-xl-4 col-lg-4 col-md-12">
                    <div class="widget clearfix">
                        <p class="text-center"><a href="<?php echo basePathUrl();?>"><img src="<?php echo basePathUrl();?>images/employeemax/Employee-Max-logo.png" alt="logo"></a></p>
                        <div class="clearfix m-b-10"></div>
                        <p class="text-center m-0">Office: 888-376-7291</p>
                        <p class="text-center m-0">Customer Service: 571-209-5356</p>
                        <div class="clearfix m-b-30"></div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-3">
                    <div class="widget">
                        <h4>Services</h4>
                        <ul class="list">
                            <li><a href="<?php echo basePathUrl();?>payroll-services">Payroll</a></li>
                            <li><a href="<?php echo basePathUrl();?>hr-services">HR</a></li>  
                            <li><a href="<?php echo basePathUrl();?>workers-compensation">Worker's Compensation</a></li> 
                            <li><a href="<?php echo basePathUrl();?>benefits">Benefits</a></li>                            
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-3">
                    <div class="widget">
                        <h4>Get to Know Us</h4>
                        <ul class="list">
                            <li><a href="<?php echo basePathUrl();?>about">About Us</a></li>
                            <li><a href="<?php echo basePathUrl();?>faqs">FAQ</a></li>
                            <li><a href="<?php echo basePathUrl();?>blog">Blog</a></li>
                            <li><a href="<?php echo basePathUrl();?>contact">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-content">
        <div class="container">
            <div class="copyright-text text-center">Copyright © <?php echo date('Y'); ?> EmployeeMax | <a href="<?php echo basePathUrl();?>privacy-policy">Privacy Policy</a> | <a href="<?php echo basePathUrl();?>terms-of-use">Terms of use</a></div>
        </div>
    </div>
</footer>